import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.awt.Font;

import javax.swing.JPanel;

public class GamePanel extends JPanel {

    // variable atau field
    private List<Benda> bendaList = new ArrayList<Benda>();
    private String playerName;

    // constructor
    public GamePanel() {
        // buat dua benda bola dan persegi
        Bola bola1 = new Bola();
        Persegi persegi = new Persegi(200, 50);
        Bola bola2 = new Bola(200, 100, 100);

        // tambahkan bola dan persegi ke array list
        bendaList.add(bola1);
        bendaList.add(persegi);
        bendaList.add(bola2);
    }

    // constructor
    public GamePanel(List<Benda> bendaList) {
        this.bendaList = bendaList;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Bola getFirstBola() {
        for (Benda benda : bendaList) {
            if (benda instanceof Bola) {
                return (Bola) benda;
            }
        }
        return null;
    }

    public Ular getFirstUlar() throws GameException {
        for (Benda benda : bendaList) {
            if (benda instanceof Ular) {
                return (Ular) benda;
            }
        }
        throw new GameException("Tidak ada ular di game");
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(800, 600);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Font fontPlayer = new Font("arial", Font.BOLD, 14);
        g.setColor(Color.RED);
        g.setFont(fontPlayer);
        g.drawString(playerName, 10, 20);

        // gambar semua benda yg ada di array
        for (Benda benda : bendaList) {
            benda.draw(g);
        }
    }
}
