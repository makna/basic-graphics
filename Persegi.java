import java.awt.Color;
import java.awt.Graphics;

public class Persegi implements Benda {
    // variable atau field
    private int x;
    private int y;
    private int panjang;
    private int lebar;
    private Color warna;

    // constructor
    public Persegi() {
        this.x = 100;
        this.y = 100;

        this.panjang = 60;
        this.lebar = 50;
    }

    public Persegi(int x, int y) {
        this.x = x;
        this.y = y;

        this.panjang = 10;
        this.lebar = 10;
    }

    public Persegi(int x, int y, int panjang, int lebar, Color warna) {
        this.x = x;
        this.y = y;
        this.panjang = panjang;
        this.lebar = lebar;
        this.warna = warna;
    }

    // method atau function
    public void draw(Graphics g) {
        g.setColor(warna);
        g.fillRect(x, y, panjang, lebar);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getPanjang() {
        return panjang;
    }

    public void setPanjang(int panjang) {
        this.panjang = panjang;
    }

    public int getLebar() {
        return lebar;
    }

    public void setLebar(int lebar) {
        this.lebar = lebar;
    }

}
