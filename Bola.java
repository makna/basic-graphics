import java.awt.Graphics;
import java.awt.Color;

public class Bola implements Benda {
    // variable atau field
    private int x;
    private int y;
    private int diameter;

    // constructor
    public Bola() {
        this.x = 0;
        this.y = 0;
        this.diameter = 50;
    }

    public Bola(int x, int y, int diameter) {
        this.x = x;
        this.y = y;
        this.diameter = diameter;
    }

    // method atau function
    public void draw(Graphics g){
        g.setColor(Color.RED);
        g.fillOval(this.x, this.y, this.diameter, this.diameter);
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int d) {
        this.diameter = d;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }
}
