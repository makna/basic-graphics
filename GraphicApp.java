import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class GraphicApp {

    private static void createAndshowGUI() {
        // buat frame
        JFrame frame = new JFrame("GUI APP");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // init beda-benda
        List<Benda> bendaList = initBenda();

        // buat panel
        GamePanel gamePanel = new GamePanel(bendaList);
        // ambil nama user dari terminal
        String playerName = getUsernameFromTerminal();
        gamePanel.setPlayerName(playerName);

        KeyboardListener keyListener = new KeyboardListener(gamePanel);
        frame.addKeyListener(keyListener);

        frame.setContentPane(gamePanel);
        frame.pack();
        frame.setVisible(true);

    }

    private static List<Benda> initBenda() {
        List<Benda> bendaList = new ArrayList<Benda>();

        // buat dua benda bola dan persegi
        // Bola bola1 = new Bola();
        // Persegi persegi = new Persegi(200, 50);
        // Bola bola2 = new Bola(200, 100, 100);

        // tambahkan bola dan persegi ke array list
        // bendaList.add(bola1);
        // bendaList.add(persegi);
        // bendaList.add(bola2);

        // Ular ular = new Ular(400, 200);
        UlarBerbisa ularBerbisa = new UlarBerbisa();
        ularBerbisa.setRacun(50);

        bendaList.add(ularBerbisa);

        return bendaList;
    }

    public static String getUsernameFromTerminal() {
        InputStreamReader inStrReader = new InputStreamReader(System.in);
        BufferedReader bufferReader = new BufferedReader(inStrReader);

        String playerName = "";

        try {
            System.out.print("Ketik nama player: ");
            playerName = bufferReader.readLine();

            // int data;
            // while ((data = inStrReader.read()) != -1) {
            // System.out.println((char) data);
            // }

            System.out.print("Nama player: ");
            System.out.println(playerName);

        } catch (IOException e) {
            // e.printStackTrace();
            System.out.println("ERROR: baca dari input, " + e.getMessage());
        }

        return playerName;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                // buat GUI
                createAndshowGUI();
            }
        });
    }
}