import java.awt.Color;
import java.awt.Graphics;

public class Ular implements Benda {
    // field variable
    private Persegi[] badanArray = new Persegi[10];
    private int x;
    private int y;
    private int idxK;
    private int idxM = 10;
    private char arah;

    // constructor
    public Ular() {
        buatBadanUlar(400, 200);
    }

    

    public Ular(int x, int y) {
        buatBadanUlar(x, y);
    }

    // method untuk buat badan ular
    private void buatBadanUlar(int x, int y) {
        this.x = x; 
        this.y = y;

        Persegi kepala = new Persegi(x, y + 00, 10, 10, Color.RED);
        Persegi badan2 = new Persegi(x, y + 10, 10, 10, Color.GREEN);
        Persegi badan3 = new Persegi(x, y + 20, 10, 10, Color.BLUE);
        Persegi badan4 = new Persegi(x, y + 30, 10, 10, Color.lightGray);
        Persegi badan5 = new Persegi(x, y + 40, 10, 10, Color.YELLOW);
        Persegi badan6 = new Persegi(x, y + 50, 10, 10, Color.PINK);
        Persegi badan7 = new Persegi(x, y + 60, 10, 10, Color.MAGENTA);
        Persegi badan8 = new Persegi(x, y + 70, 10, 10, Color.GRAY);
        Persegi badan9 = new Persegi(x, y + 80, 10, 10, Color.ORANGE);
        Persegi ekor = new Persegi(x, y + 90, 10, 10, Color.cyan);

        badanArray[0] = kepala;
        badanArray[1] = badan2;
        badanArray[2] = badan3;
        badanArray[3] = badan4;
        badanArray[4] = badan5;
        badanArray[5] = badan6;
        badanArray[6] = badan7;
        badanArray[7] = badan8;
        badanArray[8] = badan9;
        badanArray[9] = ekor;
    } 

    void maju() {
        arah = 'U';
        y = y - 10;

        for (int i = 0; i < idxM; i++) {
            int y = badanArray[i].getY();
            badanArray[i].setY(y - 10);
        }

        for (int i = idxM; i < badanArray.length; i++) {
            int x = badanArray[i].getX();
            badanArray[i].setX(x - 10);
        }
    }

    void kiri() {
        arah = 'L';
        x = x - 10;

        for (int i = 0; i < idxK; i++) {
            int x = badanArray[i].getX();
            badanArray[i].setX(x - 10);
        }

        for (int i = idxK; i < badanArray.length; i++) {
            int y = badanArray[i].getY();
            badanArray[i].setY(y - 10);
        }
        if (idxK < 10) {
            idxK++;
        }

        idxM = 0;
    }

    public Persegi[] getBadanArray() {
        return badanArray;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public char getArah() {
        return arah;
    }

    public void setArah(char arah) {
        this.arah = arah;
    }

    @Override
    public void draw(Graphics g) {
        for (int i = 0; i < badanArray.length; i++) {
            badanArray[i].draw(g);
        }
    }

}
