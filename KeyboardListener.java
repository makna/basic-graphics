import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyboardListener implements KeyListener {

    // field variable
    private GamePanel gamePanel;

    // constructor
    public KeyboardListener(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
    }

    // method atau function
    @Override
    public void keyTyped(KeyEvent e) {
        System.out.println("key typed = " + e.getKeyChar());
        try {
            // ambil ular dari game panel
            Ular ular = gamePanel.getFirstUlar();

            // handle event
            char c = e.getKeyChar();
            if (c == 'd') {
                // TODO: belum di implementasi utk belok kanan
            } else if (c == 'a') {
                ular.kiri();    
            } else if (c == 's') {
                // TODO: belum di implementasi utk ke bawah
            } else if (c == 'w') {
                ular.maju();
                
            }

            // update gambar di panel
            gamePanel.repaint();

        } catch (GameException ex) {
            System.out.println("GAME ERROR: terjadi error ketika ambil ular");
            System.out.println("GAME ERROR: " + ex.getMessage());
        } catch (NullPointerException npe) {
            System.out.println("ERROR NPE: Tidak ada ular, jadi tidak bisa kiri.");
            System.out.println("ERROR NPE: " + npe.getMessage());
        } catch (Exception ex) {
            System.out.println("SYSTEM ERROR: Terjadi error system error");
            System.out.println("SYSTEM ERROR: " + ex.getMessage());
            // ex.printStackTrace();
        } finally {
            System.out.println("FINALLY: apapun yang terjadi tetep diprint");
        }
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        // System.out.println("key pressed = " + e.getKeyChar());

    }

    @Override
    public void keyReleased(KeyEvent e) {
        // System.out.println("key released = " + e.getKeyChar());

    }

}