import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class UlarBerbisa extends Ular {
    // field variable
    private int racun;

    // constructor
    public UlarBerbisa() {
        this.setRacun(10);
    }

    // method/function
    public int getRacun() {
        return racun;
    }

    public void setRacun(int racun) {
        this.racun = racun;
    }

    @Override
    public void draw(Graphics g){
        // gambar tingkat bisa/racun
        g.setColor(Color.BLACK);
        g.setFont(new Font("Arial", Font.BOLD, 12));
        g.drawString(racun + "", getX() + 10, getY() + 10);

        System.out.println("x = " + getX());
        System.out.println("y = " + getY());
        System.out.println("arah = " + getArah());
        
        // panggil draw yang ada di parent Ular
        super.draw(g);
    }
}
